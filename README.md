# Vending Machine

## Technical Documentation

### Project Setup

#### Install Node and Npm

+ For downloading node visit [Download Node](https://markdownlivepreview.com/) .
+ Clone the project from the [gitlab](https://gitlab.com/layash.sharma/vendingmachine).
+ Go to the project. For example: `cd projects/vandingmachine`
+You will be in the master branch.
+ Run `npm install` , it will install all the prerequisite packages.
+ Run `nodemon`


##### Remarks:
+ During development:

    
|               | Versions      |
| ------------- |:-------------:|
| Node          | 12.16.1       |
| Npm           | 6.13.4        |

+  Versions can be seen on the package.json file.


##### Project Assumptions
+ No databse used but  in-memory state is used.
+ There are only three products available at the moment Coke cost (Rs 20), Pepsi cost (Rs 25),
and Dew cost (Rs 30).
+ Initial stock for items Coke 10, Pepsi 10, Dew 10 and the initial coins 100.
+ machine will accept accept one coin that amounts  to 10 but while returning  it will return coins of 5 and 10 both.
+ machine can return money while returning items.
+ Machine will work until stock of item is finished while purchases 


## API Documentation

### Inventory [/vending]

#### Get Machine Inventory [GET]

+ Response 200 (application/json)


### Purchase [/vending]

#### Purchase [POST]

+ Request (application/json)
                
    + Body
            {
            "amount": 30,
            "name": "coke",
            "payment_type": "coins",
            "price": 20,
            "return": 10,
            "type": ["10", "10", "10"]
            }



+ Response 200 (application/json)

    + Body

        {

            "success": true,
            "message": "List of items with stock and price",
            "data": {
                "items": {
                    "coke": {
                        "stock": 9,
                        "price": 20,
                        "image": "https://image.flaticon.com/icons/svg/135/135664.svg"
                    },
                    "pepsi": {
                        "stock": 10,
                        "price": 25,
                        "image": "https://image.flaticon.com/icons/svg/135/135666.svg"
                    },
                    "dew": {
                        "stock": 10,
                        "price": 30,
                        "image": "https://image.flaticon.com/icons/svg/788/788507.svg"
                    }
                },
                "payment": {
                    "coins": {
                        "5": {
                            "stock": 30,
                            "amount": 5,
                            "receive": true,
                            "return": true
                        },
                        "10": {
                            "stock": 72,
                            "amount": 10,
                            "receive": true,
                            "return": true
                        }
                    },
                    "cash": {
                        "50": {
                            "stock": 0,
                            "receive": false,
                            "amount": 50,
                            "return": false
                        },
                        "100": {
                            "stock": 0,
                            "receive": false,
                            "amount": 50,
                            "return": false
                        }
                    }
                },
                "return": {
                    "5": 0,
                    "10": 1
                },
                "return_amount": 10
            }
        }

### Refund [/vending/refund]

#### Purchase [PUT]

+ Request (application/json)
                
    + Body
            {
                amount: 0
                name: "coke"
                type: "coins"
            }



+ Response 200 (application/json)

    + Body

        {

            "success": true,
            "message": "List of items with stock and price",
            "data": {
                "items": {
                    "coke": {
                        "stock": 9,
                        "price": 20,
                        "image": "https://image.flaticon.com/icons/svg/135/135664.svg"
                    },
                    "pepsi": {
                        "stock": 10,
                        "price": 25,
                        "image": "https://image.flaticon.com/icons/svg/135/135666.svg"
                    },
                    "dew": {
                        "stock": 10,
                        "price": 30,
                        "image": "https://image.flaticon.com/icons/svg/788/788507.svg"
                    }
                },
                "payment": {
                    "coins": {
                        "5": {
                            "stock": 30,
                            "amount": 5,
                            "receive": true,
                            "return": true
                        },
                        "10": {
                            "stock": 72,
                            "amount": 10,
                            "receive": true,
                            "return": true
                        }
                    },
                    "cash": {
                        "50": {
                            "stock": 0,
                            "receive": false,
                            "amount": 50,
                            "return": false
                        },
                        "100": {
                            "stock": 0,
                            "receive": false,
                            "amount": 50,
                            "return": false
                        }
                    }
                },
                "return": {
                    "5": 0,
                    "10": 1
                },
                "return_amount": 10
            }
        }



### Thank you.
