var express = require('express');
var router = express.Router();

var testCtrl = require('../controllers/test.controller');

router.route('/').get(testCtrl.index);


module.exports = router;